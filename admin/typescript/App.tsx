import * as React from "react"
import {observer} from 'mobx-react';
import {observable, toJS} from 'mobx';

import {UserItem, User} from './User';
import {Form, NewUser} from './Form';

export class UsersCollection {
    @observable private usersCollection: User[] = [];

    public addUser(newUser: NewUser) {
        this.users.push(new User(newUser));
    }

    public deleteUser(user: User) {
        //Todo id splice
        this.usersCollection = this.users.filter(i => i.registeredOnTimestamp !== user.registeredOnTimestamp);
    }

    public get users(): User[] {
        return this.usersCollection;
    }

    public sortByField(fieldName: Column) {
        return function (a: User, b: User) {
            if (a.active === b.active) {
                if (a[fieldName] < b[fieldName])
                    return -1;
                if (a[fieldName] > b[fieldName])
                    return 1;
                return 0;
            } else if (a.active) {
                return -1;
            }
            return 1;

        }
    }
    public sort(field: Column, reverse: boolean) {
        if (this.users.length) {
            if (reverse) {
                this.usersCollection = this.users.reverse().sort(this.sortByField("active"));
            } else {
                this.usersCollection = this.users.sort(this.sortByField(field));
            }
        }
    }
}

type Column = "role" | "login" | "fullName" | "age" | "registeredOnTimestamp"|"active";


const userStore = new UsersCollection();

interface MainControllerProps {
    newUser: NewUser,
    userId: null|number,
    sortField: string

}

class MainController {
    private props = observable.object<MainControllerProps>({
        newUser: {
            role: 1,
            login: '',
            firstName: '',
            lastName: '',
            age: 0,
            password:"",
            active: true,
        },
        userId: null,
        sortField: "active"

    });

    userStore = userStore;

    public get newUser(): NewUser {return Object.assign({}, toJS(this.props.newUser));}
    public get edit(): boolean {return !!this.props.userId}

    public resetNewUser() {
        const NEW_ITEM_TEMPLATE = {
            role: 1,
            login: '',
            firstName: '',
            lastName: '',
            age: 0,
            password:"",
            active: true
        };
        Object.assign(this.props.newUser, NEW_ITEM_TEMPLATE);
    }

    changeForm<K extends keyof NewUser>(name: K, val: NewUser[K]) {
        this.props.newUser[name] = val;
    }

    removeUser(user: User) {
        this.userStore.deleteUser(user);
        this.props.userId = user.id == this.props.userId ? null : this.props.userId;
        this.resetNewUser();
    }

    editUser(user: User) {
        this.props.newUser = {...user.userFields};
        this.props.userId = user.id;
    }

    saveChanges() {
        const user = this.userStore.users.find(user => user.id === this.props.userId);
        user ? user.save(this.newUser) : console.warn(`ReferenceError: unable to find user with id ${this.props.userId}`);
        this.props.userId = null;
        this.resetNewUser();    }

    submitNewUser() {
        this.userStore.addUser(this.newUser);
        this.resetNewUser();    }



    sort(col:Column) {
        this.userStore.sort(col, col == this.props.sortField);
        this.props.sortField = col;
    }

    cancel() {
        this.resetNewUser();
        this.props.userId= null;
    }

}

const mainController = new MainController();

@observer
class Main extends React.Component<{}, {}> {

    render() {
        const users = mainController.userStore.users.map(user => {
            return <UserItem key={user.id} user={user} onRemove={() => mainController.removeUser(user)}
                             onEdit={mainController.editUser.bind(mainController)} />;
        });
        return (
            <div>
                <div>
                    <Form edit={mainController.edit} onSubmit={mainController.edit ? mainController.saveChanges.bind(mainController) : mainController.submitNewUser.bind(mainController)}
                          onChange={mainController.changeForm.bind(mainController)} newUser={mainController.newUser} onCancel={mainController.cancel.bind(mainController)}/>
                </div>
                <table>
                    <thead>
                    <tr>
                        <th onClick={() => mainController.sort("role")}>role</th>
                        <th onClick={() => mainController.sort("login")}>login</th>
                        <th onClick={() => mainController.sort("fullName")}>Full Name</th>
                        <th onClick={() => mainController.sort("age")}>age</th>
                        <th onClick={() => mainController.sort("registeredOnTimestamp")}>registered on</th>
                        <th onClick={() => mainController.sort("active")}>active</th>
                    </tr>
                    </thead>
                    <tbody>
                    {users}
                    </tbody>
                </table>
                <div>{null}</div>
            </div>
        );
    }
}

export default Main;
