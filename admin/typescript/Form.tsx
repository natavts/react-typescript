import * as React from 'react';
import {observer} from 'mobx-react';
import {observable, computed, autorun} from 'mobx';

import {Role} from './User'

type Errors<T> = {[K in keyof T]?: string}

export interface NewUser {
    role: Role,
    login: string,
    firstName: string,
    lastName: string,
    age: number,
    password: string,
    active: boolean,
}

interface FormProps {
    newUser: NewUser,
    edit: boolean,
    onSubmit(): void,
    onChange<K extends keyof NewUser>(name: K, val: NewUser[K]): void,
    onCancel(): void
}

@observer
export class Form extends React.Component<FormProps, {}> {
    @observable errors: Errors<NewUser> = {};

    public static get displayName() {return "Form";}

    componentWillReceiveProps(nextProps: FormProps) {
        // console.log(this.props.newUser, nextProps.newUser, this.props.newUser === nextProps.newUser)
        if (nextProps.newUser !== this.props.newUser) {
            this.errors = {};
        }
    }

    changeForm<K extends keyof NewUser>(name: K, val: NewUser[K]): void {
        this.props.onChange(name, val);
        this.errors = {}
    }

    onSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        this.errors = this.validator(this.props.newUser);
        if(Object.keys(this.errors).length === 0){
            this.props.onSubmit();
        }
    }

    validator(user: NewUser) {

        const {role, firstName, lastName, login, age, password} = user;

        const errors: Errors<NewUser> = {};
        if (typeof firstName !== 'string' || firstName.length < 3 || firstName.length > 15) {
            errors.firstName = "First name error";
        }
        if (typeof lastName !== 'string' || lastName.length < 3 || lastName.length > 25) {
            errors.lastName = "Last name error";
        }
        if ((age ^ 0) !== age || age < 18 || age > 55) {
            errors.age = "Age error";
        }
        const pattern = /^[a-zA-Z0-9_-]+$/;
        const matchStatus = !!pattern.exec(login);
        if (!matchStatus || !login) {
            errors.login = "Login error";
        }
        if (typeof password !== 'string' || password.length < 8) {
            errors.password = "Password error";
        }
        if ((role ^ 0) !== role) {
            errors.role = "Role error";
        }
        return errors;
    }

    render() {
        const {role, login, firstName, lastName, age, password ,active} = this.props.newUser;
        const errors = Object.values(this.errors).map((err, i) => <p key={i}>{err}</p>);
        return (
            <form onSubmit={this.onSubmit.bind(this)} className={'ContactForm'}>
                {errors}
                <select value={role} onChange={(e) => this.changeForm("role", parseInt(e.currentTarget.value, 10) as Role)} required>
                    <option value="">Nothing selected</option>
                    <option value="1">Administrator</option>
                    <option value="2">Technician</option>
                    <option value="3">Manager</option>
                    <option value="4">Supervisor</option>
                </select>

                <input type="text" placeholder="input login" value={login}
                       onChange={(e) => this.changeForm("login", e.currentTarget.value)}/>

                <input type="text" placeholder="input First Name" value={firstName}
                       onChange={(e) => this.changeForm("firstName", e.currentTarget.value)}/>

                <input type="text" placeholder="input Last Name" value={lastName}
                       onChange={(e) => this.changeForm("lastName", e.currentTarget.value)}/>

                <input type="number" placeholder="input age" value={age}
                       onChange={(e) => this.changeForm("age", +e.currentTarget.value)}/>
                <input type="password" placeholder="input password" value={password}
                       onChange={(e) => this.changeForm("password", e.currentTarget.value)}/>
                <input type="checkbox" checked={active}
                       onChange={(e) => this.changeForm("active", e.currentTarget.checked)}/>
                <input type="submit" value={ this.props.edit ? "Edit" : "Add"}/>
                {this.props.edit ? <input type="button" onClick={()=>this.props.onCancel()} value="Cancel"/> : ''}
            </form>
        );
    }

}
