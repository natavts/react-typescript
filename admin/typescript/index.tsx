import * as React from "react"
import * as ReactDOM from "react-dom"
// import App from './App';
import Main from './App';
import {UsersCollection} from './App'

ReactDOM.render(
  <Main />,
  document.getElementById('root')
);
