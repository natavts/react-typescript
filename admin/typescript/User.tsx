import * as React from 'react';

import {NewUser} from "./Form";

export type Role = 1 | 2 | 3 | 4;

let id = 0;

export class User {
    private state: NewUser;
    private registeredOn = new Date();
    public readonly id = ++id;

    constructor(data: NewUser) {
        this.state = {...data};
    }

    public get userFields(): NewUser {
        return {...this.state};
    }

    public get login(): string {
        return this.state.login;
    }

    public get age() {
        return this.state.age;
    }

    public get active() {
        return this.state.active;
    }

    public get role() {
        return this.state.role;
    }

    public get fullName(): string {
        return `${this.state.firstName} ${this.state.lastName}`;
    }

    public get registeredOnTimestamp(): number {
        return this.registeredOn.getTime();
    }

    public get registeredOnString(): string {
        return this.registeredOn.toLocaleDateString();
    }

    public get activeOnString(): string {
        return this.state.active ? 'Yes' : 'No'
    }

    public get roleCaption(): string {
        return User.roles[this.state.role];
    }

    public static readonly roles = {
        1: "Administrator",
        2: "Technician",
        3: "Manager",
        4: "Supervisor"
    };

    public save(newUser:NewUser) {
        // Object.assign(this, newUser);
        this.state = {...newUser};
    }
}

interface UserItemProps {
    user: User,
    onRemove(user: User): void,
    onEdit(user: User): void,
}


export const UserItem = ({user, onEdit, onRemove}: UserItemProps) => {
    const {login, fullName, registeredOnString, roleCaption, activeOnString, age} = user;
    return (
        <tr>
            <td>{roleCaption}</td>
            <td>{login}</td>
            <td>{fullName}</td>
            <td>{age}</td>
            <td>{registeredOnString}</td>
            <td>{activeOnString}</td>
            <td><button onClick={() => onEdit(user)}>edit</button></td>
            <td><button onClick={() => onRemove(user)}>delete</button></td>
        </tr>
    );
};