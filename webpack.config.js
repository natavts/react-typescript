const uglify = require('./node_modules/webpack/lib/optimize/UglifyJsPlugin');
const options = {
    compress: { warnings: false },
    output: { comments: false },
    sourceMap: true
}

module.exports = {
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: "ts-loader"
            },
            {
                test: /\.js?$/,
                enforce: "pre",
                use: "source-map-loader"
            }
        ]
    },
    plugins: [].concat(!process.argv.find(a => a === "-p") ? [] : new uglify(options)),
    resolve: {
        extensions: [
            ".tsx",
            ".ts",
            ".js"
        ]
    },
    stats: {
        children: false,
        chunks: false,
        errorDetails: false,
        modules: false,
        publicPath: false,
        reasons: false,
        source: false,
        warnings: false
    },
    watchOptions: {
        ignored: /node_modules/
    }
};